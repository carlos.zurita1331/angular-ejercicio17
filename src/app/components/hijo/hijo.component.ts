import { outputAst } from '@angular/compiler';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {



  constructor() { }
  
  mensaje: string = 'Mensaje del hijo al padre';
  mensaje2: string = 'ya voy camino al mercado';
  mensaje3: string = 'ok, si no encuentro pan llevo, cereales';
  mensaje4: string = 'ya no me quiere fiar por moroso';
  mensaje5: string = 'llegare a las 1 pm aproximadamente'

  @Output() EventoMensaje = new EventEmitter<string>();
  @Output() EventoMensaje2 = new EventEmitter<string>();
  @Output() EventoMensaje3 = new EventEmitter<string>();
  @Output() EventoMensaje4 = new EventEmitter<string>();
  @Output() EventoMensaje5 = new EventEmitter<string>();

  ngOnInit(): void {
    this.EventoMensaje.emit(this.mensaje);
    this.EventoMensaje2.emit(this.mensaje2)
    this.EventoMensaje3.emit(this.mensaje3)
    this.EventoMensaje4.emit(this.mensaje4)
    this.EventoMensaje5.emit(this.mensaje5)
  }

}
